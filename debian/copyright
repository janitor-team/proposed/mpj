Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: MPJ Express
Upstream-Contact: mpj-user@lists.reading.ac.uk
Source: http://sourceforge.net/projects/mpjexpress/files/
Files-Excluded:
    bin/*.bat
    *.pdf
    lib/*
    THIRDPARTYLICENSES.txt
    README-win.txt

Files: *
Copyright: 2004 - 2007 Distributed Systems Group, University of Portsmouth,
                       Aamir Shafi <aamir.shafi@seecs.edu.pk>
                       Bryan Carpenter <bryan.carpenter@port.ac.uk>
                       Mark Baker <mark.baker@computer.org>
                       Guillermo Lopez Taboada <taboada@udc.es>
License: MIT

License: MIT
 The bulk of code in this distribution was developed by the Distributed Systems
 Group at the University of Portsmouth. Some sections of the code like
 the buffering API and derived datatypes include contributions developed at
 the Community Grids Lab at Indiana University.
 .
 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:
 .
 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
 NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR
 THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Files: debian/*
Copyright: © 2012 Andreas Tille <tille@debian.org>
License: MIT
